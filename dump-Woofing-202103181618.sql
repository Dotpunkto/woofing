-- MySQL dump 10.13  Distrib 8.0.23, for macos10.15 (x86_64)
--
-- Host: localhost    Database: Woofing
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Activity`
--

DROP TABLE IF EXISTS `Activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Activity` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Activity` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Activity`
--

LOCK TABLES `Activity` WRITE;
/*!40000 ALTER TABLE `Activity` DISABLE KEYS */;
INSERT INTO `Activity` VALUES (1,'Food Chemist'),(2,'Quality Engineer'),(3,'Web Designer I'),(4,'Human Resources Manager'),(5,'Speech Pathologist'),(6,'Help Desk Technician'),(7,'Desktop Support Technician'),(8,'Web Designer IV'),(9,'Electrical Engineer'),(10,'Sales Associate'),(11,'Tax Accountant'),(12,'VP Accounting'),(13,'Information Systems Manager'),(14,'GIS Technical Architect'),(15,'Nurse'),(16,'Food Chemist'),(17,'Assistant Manager'),(18,'Database Administrator III'),(19,'Information Systems Manager'),(20,'Director of Sales');
/*!40000 ALTER TABLE `Activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Activity_Task`
--

DROP TABLE IF EXISTS `Activity_Task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Activity_Task` (
  `Task` int NOT NULL,
  `Activity` int NOT NULL,
  KEY `Activity_Task_FK` (`Task`),
  KEY `Activity_Task_FK_1` (`Activity`),
  CONSTRAINT `Activity_Task_FK` FOREIGN KEY (`Task`) REFERENCES `Task` (`ID`),
  CONSTRAINT `Activity_Task_FK_1` FOREIGN KEY (`Activity`) REFERENCES `Activity` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Activity_Task`
--

LOCK TABLES `Activity_Task` WRITE;
/*!40000 ALTER TABLE `Activity_Task` DISABLE KEYS */;
INSERT INTO `Activity_Task` VALUES (1,1),(2,2),(3,3),(4,4),(6,6),(7,7),(9,9),(10,10),(11,11),(13,13),(14,14),(15,15),(16,16),(17,17),(18,18),(20,20);
/*!40000 ALTER TABLE `Activity_Task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Demande`
--

DROP TABLE IF EXISTS `Demande`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Demande` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Approve` tinyint(1) NOT NULL,
  `Volunteer` int NOT NULL,
  `Task` int DEFAULT NULL,
  `Presence` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Demande_FK` (`Task`),
  KEY `Demande_FK_2` (`Volunteer`),
  CONSTRAINT `Demande_FK` FOREIGN KEY (`Task`) REFERENCES `Task` (`ID`),
  CONSTRAINT `Demande_FK_2` FOREIGN KEY (`Volunteer`) REFERENCES `User` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Demande`
--

LOCK TABLES `Demande` WRITE;
/*!40000 ALTER TABLE `Demande` DISABLE KEYS */;
INSERT INTO `Demande` VALUES (1,1,12,14,1),(7,0,8,15,1),(8,0,10,14,0),(9,0,5,14,0),(11,1,14,2,0),(14,0,2,2,1),(15,1,12,14,1),(16,0,13,2,0),(18,1,4,15,1),(19,1,11,2,1),(20,1,12,15,1),(21,0,12,4,0),(22,0,12,4,0);
/*!40000 ALTER TABLE `Demande` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Site`
--

DROP TABLE IF EXISTS `Site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Site` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `City` varchar(255) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `Number` varchar(255) NOT NULL,
  `Capacity` int NOT NULL,
  `Activity` int NOT NULL,
  `Host` int NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `Site_FK_1` (`Host`),
  KEY `Site_FK` (`Activity`),
  CONSTRAINT `Site_FK` FOREIGN KEY (`Activity`) REFERENCES `Site_Activity` (`ID`),
  CONSTRAINT `Site_FK_1` FOREIGN KEY (`Host`) REFERENCES `User` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Site`
--

LOCK TABLES `Site` WRITE;
/*!40000 ALTER TABLE `Site` DISABLE KEYS */;
INSERT INTO `Site` VALUES (1,'Lille','664 Evergreen Court','841',3,3,1),(2,'Anda','9881 Butternut Drive','09',10,5,3),(3,'Phoenix','6431 Center Alley','392',7,6,4),(4,'Namikupa','81 West Plaza','3',5,1,5),(5,'Miaoyu','32 International Hill','97',4,1,6),(6,'Cimarelang','168 Memorial Plaza','10504',3,1,7),(7,'Luyang','869 Lawn Terrace','2',10,4,9),(8,'Xinhuang','3 Loeprich Hill','3388',3,4,10),(9,'Tenkodogo','237 Milwaukee Lane','6493',7,1,11),(10,'Almere Stad','9993 Carioca Junction','40',5,4,13);
/*!40000 ALTER TABLE `Site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Site_Activity`
--

DROP TABLE IF EXISTS `Site_Activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Site_Activity` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Type` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Site_Activity`
--

LOCK TABLES `Site_Activity` WRITE;
/*!40000 ALTER TABLE `Site_Activity` DISABLE KEYS */;
INSERT INTO `Site_Activity` VALUES (1,'adipiscing'),(2,'porttitor'),(3,'pellentesque'),(4,'suspendisse'),(5,'cursus'),(6,'vulputate');
/*!40000 ALTER TABLE `Site_Activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Task`
--

DROP TABLE IF EXISTS `Task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Task` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Desciption` varchar(255) NOT NULL,
  `DateStart` datetime NOT NULL,
  `DateEnd` datetime NOT NULL,
  `Capacity` int NOT NULL,
  `Host` int DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Task_FK` (`Host`),
  CONSTRAINT `Task_FK` FOREIGN KEY (`Host`) REFERENCES `User` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Task`
--

LOCK TABLES `Task` WRITE;
/*!40000 ALTER TABLE `Task` DISABLE KEYS */;
INSERT INTO `Task` VALUES (1,'Bamity','nulla sed vel enim sit amet nunc','2021-01-16 00:00:00','2021-04-22 23:59:59',14,1),(2,'Voltsillam','elit sodales scelerisque mauris sit amet eros suspendisse accumsan','2020-09-14 00:00:00','2021-04-14 23:59:59',8,4),(3,'Cardify','libero quis orci nullam molestie nibh in lectus','2020-12-26 00:00:00','2021-04-12 23:59:59',2,1),(4,'Greenlam','euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula','2021-01-07 00:00:00','2021-04-21 23:59:59',8,3),(6,'Home Ing','pulvinar nulla pede ullamcorper augue a suscipit nulla elit','2021-03-24 00:00:00','2021-04-07 23:59:59',2,5),(7,'Sonsing','posuere cubilia curae mauris viverra','2020-10-08 00:00:00','2021-04-16 23:59:59',2,6),(9,'Ventosanzap','leo odio porttitor id consequat in consequat ut nulla','2020-09-01 00:00:00','2021-03-31 23:59:59',8,7),(10,'Konklab','vitae consectetuer eget rutrum at lorem integer','2020-08-15 00:00:00','2021-04-07 23:59:59',14,3),(11,'Daltfresh','nam congue risus semper porta volutpat quam pede lobortis 00:00:00','2020-04-28 00:00:00','2021-03-31 23:59:59',2,1),(13,'Lotstring','sit amet eros suspendisse accumsan tortor quis turpis sed','2020-12-31 00:00:00','2021-04-11 23:59:59',14,3),(14,'It','amet diam in magna bibendum imperdiet nullam','2020-07-06 00:00:00','2021-04-10 23:59:59',15,1),(15,'Cookley','eget vulputate ut ultrices vel augue vestibulum ante','2020-09-27 00:00:00','2021-04-04 23:59:59',15,13),(16,'Andalax','dui vel sem sed sagittis nam','2021-02-04 00:00:00','2021-04-04 23:59:59',2,3),(17,'Fixflex','in tempor turpis nec euismod','2020-11-17 00:00:00','2021-04-05 23:59:59',2,10),(18,'Holdlamis','ligula suspendisse ornare consequat lectus in est','2020-05-08 00:00:00','2021-04-13 23:59:59',5,1),(20,'Cookley','lectus in est risus auctor sed','2020-10-19 00:00:00','2021-04-07 23:59:59',2,5);
/*!40000 ALTER TABLE `Task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `User` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Username` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Host` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (1,'dchidlow0','YnEArTXmb',1),(2,'rwhitebrook1','OHXCINPF4',0),(3,'eblanc2','ziumG6ND',1),(4,'ggreeding3','1PQ8avQl',1),(5,'ataber4','OpyU1B9qAxd8',1),(6,'rgianilli5','zU9NaGzO',1),(7,'dpalffrey6','AFH91xqOC',1),(8,'edocwra7','jO8ZVsiRPX',0),(9,'cbalderston8','6NrKt3P',1),(10,'dwestmacott9','R5Nv8L',1),(11,'ycomera','ug6QnEVPg1l',1),(12,'kshillabeareb','gu1Wig',0),(13,'caldcornec','IH7qyUSI',1),(14,'rmacgaugheyd','RXt1qfs8z7bw',0),(15,'bbearde','P4aAQzjRc6QR',0);
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'Woofing'
--
/*!50003 DROP PROCEDURE IF EXISTS `Remove_Task` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Remove_Task`(IN id INT)
BEGIN
	
	START TRANSACTION;
	
	DELETE Activity_Task, Demande FROM Task 
		LEFT JOIN Activity_Task  ON Task.ID = Activity_Task.Task 
		LEFT JOIN Demande ON Task.ID = Demande.Task
		WHERE Task.ID = id;
	DELETE FROM Task WHERE Task.ID = id;
	
	COMMIT;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Skills_Level` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Skills_Level`(in id INT)
BEGIN
	
	SELECT a.Activity, COUNT(1) Level FROM Demande d 
		LEFT JOIN Task t ON d.Task = t.ID 
		LEFT JOIN Activity_Task a_t ON t.ID = a_t.Task 
		LEFT JOIN Activity a ON a.ID = a_t.Activity 
		WHERE d.Volunteer = id and d.Approve = TRUE AND d.Presence = TRUE 
		GROUP BY a.Activity;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `User_Task` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `User_Task`(IN id INT, IN Task INT)
BEGIN
	INSERT INTO Demande (Approve, Volunteer, Task, Presence) Values (0, id, Task, 0);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-18 16:18:13
