DELIMITER;;
CREATE DEFINER = `root` @`localhost` PROCEDURE `Remove_Task`(IN id INT) BEGIN START TRANSACTION;
DELETE Activity_Task,
Demande
FROM
  Task
  LEFT JOIN Activity_Task ON Task.ID = Activity_Task.Task
  LEFT JOIN Demande ON Task.ID = Demande.Task
WHERE
  Task.ID = id;
DELETE FROM
  Task
WHERE
  Task.ID = id;
COMMIT;
END;;
DELIMITER;