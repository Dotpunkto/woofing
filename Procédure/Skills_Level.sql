DELIMITER;;
CREATE DEFINER = `root` @`localhost` PROCEDURE `Skills_Level`(in id INT) BEGIN
SELECT
  a.Activity,
  COUNT(1) Level
FROM
  Demande d
  LEFT JOIN Task t ON d.Task = t.ID
  LEFT JOIN Activity_Task a_t ON t.ID = a_t.Task
  LEFT JOIN Activity a ON a.ID = a_t.Activity
WHERE
  d.Volunteer = id
  and d.Approve = TRUE
  AND d.Presence = TRUE
GROUP BY
  a.Activity;
END;;
DELIMITER;