INSERT INTO
  `Activity`
VALUES
  (1, 'Food Chemist'),
  (2, 'Quality Engineer'),
  (3, 'Web Designer I'),
  (4, 'Human Resources Manager'),
  (5, 'Speech Pathologist'),
  (6, 'Help Desk Technician'),
  (7, 'Desktop Support Technician'),
  (8, 'Web Designer IV'),
  (9, 'Electrical Engineer'),
  (10, 'Sales Associate'),
  (11, 'Tax Accountant'),
  (12, 'VP Accounting'),
  (13, 'Information Systems Manager'),
  (14, 'GIS Technical Architect'),
  (15, 'Nurse'),
  (16, 'Food Chemist'),
  (17, 'Assistant Manager'),
  (18, 'Database Administrator III'),
  (19, 'Information Systems Manager'),
  (20, 'Director of Sales');
INSERT INTO
  `Activity_Task`
VALUES
  (1, 1),
  (2, 2),
  (3, 3),
  (4, 4),
  (6, 6),
  (7, 7),
  (9, 9),
  (10, 10),
  (11, 11),
  (13, 13),
  (14, 14),
  (15, 15),
  (16, 16),
  (17, 17),
  (18, 18),
  (20, 20);
INSERT INTO
  `Demande`
VALUES
  (1, 1, 12, 14, 1),
  (7, 0, 8, 15, 1),
  (8, 0, 10, 14, 0),
  (9, 0, 5, 14, 0),
  (11, 1, 14, 2, 0),
  (14, 0, 2, 2, 1),
  (15, 1, 12, 14, 1),
  (16, 0, 13, 2, 0),
  (18, 1, 4, 15, 1),
  (19, 1, 11, 2, 1),
  (20, 1, 12, 15, 1),
  (21, 0, 12, 4, 0),
  (22, 0, 12, 4, 0);
INSERT INTO
  `Site`
VALUES
  (
    1,
    'Lille',
    '664 Evergreen Court',
    '841',
    3,
    3,
    1
  ),(
    2,
    'Anda',
    '9881 Butternut Drive',
    '09',
    10,
    5,
    3
  ),(
    3,
    'Phoenix',
    '6431 Center Alley',
    '392',
    7,
    6,
    4
  ),(4, 'Namikupa', '81 West Plaza', '3', 5, 1, 5),(
    5,
    'Miaoyu',
    '32 International Hill',
    '97',
    4,
    1,
    6
  ),(
    6,
    'Cimarelang',
    '168 Memorial Plaza',
    '10504',
    3,
    1,
    7
  ),(7, 'Luyang', '869 Lawn Terrace', '2', 10, 4, 9),(
    8,
    'Xinhuang',
    '3 Loeprich Hill',
    '3388',
    3,
    4,
    10
  ),(
    9,
    'Tenkodogo',
    '237 Milwaukee Lane',
    '6493',
    7,
    1,
    11
  ),(
    10,
    'Almere Stad',
    '9993 Carioca Junction',
    '40',
    5,
    4,
    13
  );
INSERT INTO
  `Site_Activity`
VALUES
  (1, 'adipiscing'),
  (2, 'porttitor'),
  (3, 'pellentesque'),
  (4, 'suspendisse'),
  (5, 'cursus'),
  (6, 'vulputate');
INSERT INTO
  `Task`
VALUES
  (
    1,
    'Bamity',
    'nulla sed vel enim sit amet nunc',
    '2021-01-16 00:00:00',
    '2021-04-22 23:59:59',
    14,
    1
  ),(
    2,
    'Voltsillam',
    'elit sodales scelerisque mauris sit amet eros suspendisse accumsan',
    '2020-09-14 00:00:00',
    '2021-04-14 23:59:59',
    8,
    4
  ),(
    3,
    'Cardify',
    'libero quis orci nullam molestie nibh in lectus',
    '2020-12-26 00:00:00',
    '2021-04-12 23:59:59',
    2,
    1
  ),(
    4,
    'Greenlam',
    'euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula',
    '2021-01-07 00:00:00',
    '2021-04-21 23:59:59',
    8,
    3
  ),(
    6,
    'Home Ing',
    'pulvinar nulla pede ullamcorper augue a suscipit nulla elit',
    '2021-03-24 00:00:00',
    '2021-04-07 23:59:59',
    2,
    5
  ),(
    7,
    'Sonsing',
    'posuere cubilia curae mauris viverra',
    '2020-10-08 00:00:00',
    '2021-04-16 23:59:59',
    2,
    6
  ),(
    9,
    'Ventosanzap',
    'leo odio porttitor id consequat in consequat ut nulla',
    '2020-09-01 00:00:00',
    '2021-03-31 23:59:59',
    8,
    7
  ),(
    10,
    'Konklab',
    'vitae consectetuer eget rutrum at lorem integer',
    '2020-08-15 00:00:00',
    '2021-04-07 23:59:59',
    14,
    3
  ),(
    11,
    'Daltfresh',
    'nam congue risus semper porta volutpat quam pede lobortis 00:00:00',
    '2020-04-28 00:00:00',
    '2021-03-31 23:59:59',
    2,
    1
  ),(
    13,
    'Lotstring',
    'sit amet eros suspendisse accumsan tortor quis turpis sed',
    '2020-12-31 00:00:00',
    '2021-04-11 23:59:59',
    14,
    3
  ),(
    14,
    'It',
    'amet diam in magna bibendum imperdiet nullam',
    '2020-07-06 00:00:00',
    '2021-04-10 23:59:59',
    15,
    1
  ),(
    15,
    'Cookley',
    'eget vulputate ut ultrices vel augue vestibulum ante',
    '2020-09-27 00:00:00',
    '2021-04-04 23:59:59',
    15,
    13
  ),(
    16,
    'Andalax',
    'dui vel sem sed sagittis nam',
    '2021-02-04 00:00:00',
    '2021-04-04 23:59:59',
    2,
    3
  ),(
    17,
    'Fixflex',
    'in tempor turpis nec euismod',
    '2020-11-17 00:00:00',
    '2021-04-05 23:59:59',
    2,
    10
  ),(
    18,
    'Holdlamis',
    'ligula suspendisse ornare consequat lectus in est',
    '2020-05-08 00:00:00',
    '2021-04-13 23:59:59',
    5,
    1
  ),(
    20,
    'Cookley',
    'lectus in est risus auctor sed',
    '2020-10-19 00:00:00',
    '2021-04-07 23:59:59',
    2,
    5
  );
INSERT INTO
  `User`
VALUES
  (1, 'dchidlow0', 'YnEArTXmb', 1),
  (2, 'rwhitebrook1', 'OHXCINPF4', 0),
  (3, 'eblanc2', 'ziumG6ND', 1),
  (4, 'ggreeding3', '1PQ8avQl', 1),
  (5, 'ataber4', 'OpyU1B9qAxd8', 1),
  (6, 'rgianilli5', 'zU9NaGzO', 1),
  (7, 'dpalffrey6', 'AFH91xqOC', 1),
  (8, 'edocwra7', 'jO8ZVsiRPX', 0),
  (9, 'cbalderston8', '6NrKt3P', 1),
  (10, 'dwestmacott9', 'R5Nv8L', 1),
  (11, 'ycomera', 'ug6QnEVPg1l', 1),
  (12, 'kshillabeareb', 'gu1Wig', 0),
  (13, 'caldcornec', 'IH7qyUSI', 1),
  (14, 'rmacgaugheyd', 'RXt1qfs8z7bw', 0),
  (15, 'bbearde', 'P4aAQzjRc6QR', 0);