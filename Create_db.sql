CREATE DATABASE `Woofing`;
DROP TABLE IF EXISTS `Activity`;
CREATE TABLE `Activity` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Activity` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE = InnoDB AUTO_INCREMENT = 21 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci;
DROP TABLE IF EXISTS `Activity_Task`;
CREATE TABLE `Activity_Task` (
  `Task` int NOT NULL,
  `Activity` int NOT NULL,
  KEY `Activity_Task_FK` (`Task`),
  KEY `Activity_Task_FK_1` (`Activity`),
  CONSTRAINT `Activity_Task_FK` FOREIGN KEY (`Task`) REFERENCES `Task` (`ID`),
  CONSTRAINT `Activity_Task_FK_1` FOREIGN KEY (`Activity`) REFERENCES `Activity` (`ID`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci;
DROP TABLE IF EXISTS `Demande`;
CREATE TABLE `Demande` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Approve` tinyint(1) NOT NULL,
  `Volunteer` int NOT NULL,
  `Task` int DEFAULT NULL,
  `Presence` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Demande_FK` (`Task`),
  KEY `Demande_FK_2` (`Volunteer`),
  CONSTRAINT `Demande_FK` FOREIGN KEY (`Task`) REFERENCES `Task` (`ID`),
  CONSTRAINT `Demande_FK_2` FOREIGN KEY (`Volunteer`) REFERENCES `User` (`ID`)
) ENGINE = InnoDB AUTO_INCREMENT = 23 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci;
DROP TABLE IF EXISTS `Site`;
CREATE TABLE `Site` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `City` varchar(255) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `Number` varchar(255) NOT NULL,
  `Capacity` int NOT NULL,
  `Activity` int NOT NULL,
  `Host` int NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `Site_FK_1` (`Host`),
  KEY `Site_FK` (`Activity`),
  CONSTRAINT `Site_FK` FOREIGN KEY (`Activity`) REFERENCES `Site_Activity` (`ID`),
  CONSTRAINT `Site_FK_1` FOREIGN KEY (`Host`) REFERENCES `User` (`ID`)
) ENGINE = InnoDB AUTO_INCREMENT = 11 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci;
DROP TABLE IF EXISTS `Site_Activity`;
CREATE TABLE `Site_Activity` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Type` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE = InnoDB AUTO_INCREMENT = 7 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci;
DROP TABLE IF EXISTS `Task`;
CREATE TABLE `Task` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Desciption` varchar(255) NOT NULL,
  `DateStart` datetime NOT NULL,
  `DateEnd` datetime NOT NULL,
  `Capacity` int NOT NULL,
  `Host` int DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Task_FK` (`Host`),
  CONSTRAINT `Task_FK` FOREIGN KEY (`Host`) REFERENCES `User` (`ID`)
) ENGINE = InnoDB AUTO_INCREMENT = 21 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci;
DROP TABLE IF EXISTS `User`;
CREATE TABLE `User` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Username` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Host` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE = InnoDB AUTO_INCREMENT = 16 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci;